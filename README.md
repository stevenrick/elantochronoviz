#elanToChronoViz

This python script converts annotations created in ELAN and exported as tab-delimited textinto a format compatible for importing into [ChronoViz](http://chronoviz.com/).


##Input Structure Requirements

The script is flexible in regards to annotation file structure, but it does assume the file was exported using the follow settings:

![export](https://bytebucket.org/srick/elantochronoviz/raw/932c8b2b3e95afbcdc7ca86c870b10392f468985/exportsettings.png)


##Using elanToChronoViz

The only requirement is to have Python 2.7 installed.

To run the application enter:
python elanToChronoViz.py

Once open, a file dialogue will open asking you to select the file you wish to convert.


##Output

The output file will match the name of the input file but be in .csv format


##Importing into ChronoViz

Once the output is generated, you can import it into ChronoViz by using the Add Data option. Select the file and in the pop up window make certain that the Time Coding is relative, Time Column is StartTime,
 and the lower data columns match the following:
 ![import](https://bytebucket.org/srick/psyscopetochronoviz/raw/70c58fea7e9ce278d5a7557de62d7164db84fb0b/import.png)