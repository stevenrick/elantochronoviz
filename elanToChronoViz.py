'''
Created on May 5, 2014

@author: rick
'''

import re
import csv
import os
import Tkinter, tkFileDialog


def is_number(s):
    try:
        float(s)
        return True
    except ValueError:
        return False


def clean():
    print "Starting clean"
    
    # define options for opening or saving a file
    
    filename = tkFileDialog.askopenfilename()
    savename = os.path.splitext(filename)[0]+"_temp.csv"
    
    txt_file = r""+filename+""
    csv_file = r""+savename+""
    
    with open(txt_file, 'rb') as inFile, open(csv_file,'wb') as temp_csvOutput:
        
        for line in inFile:
            #print line
            clean_pattern = re.compile(r'[\t\s  a-zA-Z0-9\-\+\*\(\)_:.,]+') #get rid of garbage unicode
            fixed = clean_pattern.findall(line)
            corrected = ''.join(fixed)
            columns = corrected.split('\t') #split columns by tabs
            newColumn = []
            for el in columns:
                if el == ',':
                    print el
                    newEl = '","'
                    print newEl
                    newColumn.append(newEl)
                else:
                    newColumn.append(el)
            #print columns
            temp_csvOutput.write( ','.join(newColumn) )
    
    inFile.close()
    temp_csvOutput.close()
    print "Finished clean"
    return savename


def convert(tempfile):
    savename = (os.path.splitext(tempfile)[0])[:-5]+".csv"
    temp_file = r""+tempfile+""
    annotation_file = r""+savename+""
    with open(temp_file, "rU") as in_file, open(annotation_file, 'wb') as out_file:
        inReader = csv.reader(in_file, delimiter = ',')
        outWriter = csv.writer(out_file, delimiter=',')
        
        #write the file header
        header = 'StartTime,EndTime,Title,Annotation,MainCategory,Category2,Category3'
        writeholder = header+'\n' 
        outWriter.writerow(writeholder.split(','))
        
        headerFlag = True
        header = []
        #category = 'Category 1'
        print 'Starting conversion'
        
        for row in inReader:
            print row
            if headerFlag:
                for el in row:
                    header.append(el)
                headerFlag = False
                last = (len(header))-1
                header[last] = (header[last])[:-1]
                print header
                continue
            for el in row[2:]:
                if el != '' and el != '\n':
                    #print repr(el) +":" + str(row.index(el))
                    startTime = row[0]
                    endTime = row[1]
                    annotation = el
                    column = row.index(el) 
                    category = header[column]
                    writeHolder = str(startTime)+','+str(endTime)+',,'+annotation+','+category
                    outWriter.writerow(writeHolder.split(','))
            
    print 'Conversion complete'
    out_file.close()
    in_file.close()
    os.remove(tempfile)


root = Tkinter.Tk()
root.withdraw()

tempfile = clean() #function to convert raw elan output from tab separated .txt to .csv
convert(tempfile) #function to convert .csv into ChronoViz annotation format